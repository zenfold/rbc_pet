curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "id": 0,
  "category": {
    "id": 0,
    "name": "string"
  },
  "name": "doggie",
  "photoUrls": [
    "string"
  ],
  "tags": [
    {
      "id": 0,
      "name": "string"
    }
  ],
  "status": "available"
}' 'http://localhost:8080/pet'
