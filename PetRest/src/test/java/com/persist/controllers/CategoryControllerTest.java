package com.persist.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.persist.Application;
import com.persist.model.Category;
import com.persist.model.CategoryRepository;
import com.persist.model.PetRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class CategoryControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		// this.petRepository.deleteAllInBatch();
		// this.categoryRepository.deleteAllInBatch();

		System.out.println("Intializing Category");
		Arrays.asList("Dog,Cat,Snake,Bird,Lizard,Frog,Hamster".split(",")).forEach(a -> {
			Category category = categoryRepository.save(new Category(a));
		});

		/*
		 * System.out.println("Intializing Pet"); String tempString = new
		 * String("What is this?"); Collection<Category> categories =
		 * categoryRepository.findAll();
		 * System.out.println("What is the count: " + categories.size());
		 * 
		 * final Category[] cats = categories.toArray(new Category[0]); int n =
		 * categories.size(), count = 0; Collection<String> values =
		 * Arrays.asList("Spot,Rover,Charlie".split(",")); Set<String> photos =
		 * new HashSet<String>();
		 * 
		 * photos.add("photo1"); photos.add("photo2"); photos.add("photo3");
		 * photos.add("photo4");
		 * 
		 * boolean toggle = false; for(String value: values ) { Pet pet = new
		 * Pet(value); pet.setCategory(cats[(++count)%n]);
		 * pet.setPhotoUrls(photos); if(toggle) {
		 * pet.setPetStatus(PetStatus.available); } else {
		 * pet.setPetStatus(PetStatus.unavailable); } toggle = !toggle;
		 * petRepository.save(pet); }
		 */
	}

	@Test
	public void getAllCategories() throws Exception {
		mockMvc.perform(get("/category")).andExpect(status().isOk()).andExpect(content().contentType(contentType));
	}

	/*
	 * 
	 * 
	 * 
	 * curl -X POST --header 'Content-Type: application/json' --header 'Accept:
	 * application/json' -d '{ "id": 0, "category": { "id": 0, "name": "string"
	 * }, "name": "doggie", "photoUrls": [ "string" ], "tags": [ { "id": 0,
	 * "name": "string" } ], "status": "available" }'
	 * 'http://petstore.swagger.io/v2/pet'
	 * 
	 * 
	 * 
	 * 
	@Test
	public void createCategory() throws Exception {
		String categoryJson = json(new Category("Did you drink the water?"));
		this.mockMvc.perform(post("/category/").contentType(contentType).content(categoryJson))
				.andExpect(status().isCreated());
	}
	 */

}
