package com.persist;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.persist.model.Account;
import com.persist.model.AccountRepository;
import com.persist.model.Category;
import com.persist.model.CategoryRepository;
import com.persist.model.Pet;
import com.persist.model.Pet.PetStatus;
import com.persist.model.PetRepository;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	PetRepository petRepository;

	@Bean
	ServletRegistrationBean h2servletRegistration() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
		registrationBean.addUrlMappings("/console/*");
		return registrationBean;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Intializing Category");
		Arrays.asList("Dog,Cat,Snake,Bird,Lizard,Frog,Hamster".split(",")).forEach(a -> {
			Category category = categoryRepository.save(new Category(a));
		});

		Arrays.asList("jhoeller,dsyer,pwebb,ogierke,rwinch,mfisher,mpollack,jlong".split(",")).forEach(a -> {
			Account account = accountRepository.save(new Account(a, "password"));
		});
		System.out.println("Intializing Pet");
		String tempString = new String("What is this?");
		Collection<Category> categories = categoryRepository.findAll();
		System.out.println("What is the count: " + categories.size());

		final Category[] cats = categories.toArray(new Category[0]);
		int n = categories.size(), count = 0;
		Collection<String> values = Arrays.asList("Spot,Rover,Charlie".split(","));
		Set<String> photos = new HashSet<String>();

		photos.add("photo1");
		photos.add("photo2");
		photos.add("photo3");
		photos.add("photo4");

		boolean toggle = false;
		for (String value : values) {
			Pet pet = new Pet(value);
			pet.setCategory(cats[(++count) % n]);
			pet.setPhotoUrls(photos);
			if (toggle) {
				pet.setPetStatus(PetStatus.available);
			} else {
				pet.setPetStatus(PetStatus.unavailable);
			}
			toggle = !toggle;
			petRepository.save(pet);
		}

	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
